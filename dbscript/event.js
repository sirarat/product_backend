const mongoose = require('mongoose')
const Event = require('../models/Event')
mongoose.connect('mongodb://localhost:27017/example')
async function clear () {
  await Event.deleteMany({})
}
async function main () {
  await clear()
  await Event.insertMany([
    {
      title: 'Title 1', content: 'content 1', startDate: new Date('2022-03-03 08:00'), endDate: new Date('2022-03-03 16:00'), class: 'a'
    },
    {
      title: 'Title 2', content: 'content 2', startDate: new Date('2022-03-30 08:00'), endDate: new Date('2022-03-30 16:00'), class: 'a'
    },
    {
      title: 'Title 3', content: 'content 3', startDate: new Date('2022-03-20 08:00'), endDate: new Date('2022-03-20 16:00'), class: 'c'
    },
    {
      title: 'Title 4', content: 'content 4', startDate: new Date('2022-03-21 08:00'), endDate: new Date('2021-03-21 12:00'), class: 'b'
    },
    {
      title: 'Title 5', content: 'content 5', startDate: new Date('2022-03-21 13:00'), endDate: new Date('2022-03-21 16:00'), class: 'c'
    },
    {
      title: 'Title 6', content: 'content 6', startDate: new Date('2022-03-27 13:00'), endDate: new Date('2022-03-27 16:00'), class: 'c'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('finish')
})
